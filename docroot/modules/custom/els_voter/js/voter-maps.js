(function ($) {
  'use strict';
  //marker clusterer
  var mc;

  /**
   * Provides the callback that is called when maps loads.
   */
  function initialize(drupalSettings) {
    // Wait until the window load event to try to use the maps library.
    $(document).ready(function (e) {
      var center = {lat: 4.570868, lng: -74.29733299999998};
      var voters = drupalSettings.geolocation.voters;
      var locations = [];
      if(typeof voters === "undefined") {
        return false;
      }

      voters.forEach(function(voter) {
        var info_voter = voter.name+ '<br>' + voter.email + '<br>' + voter.location_value + '<br>' + voter.mobile_phone;
        locations.push([
          info_voter,
          voter.lat,
          voter.lng
        ]);
      });
      var map = new google.maps.Map(document.getElementById('voters-map'), {
        zoom: 10,
        center: center
      });
      mc = new MarkerClusterer(map, [], {
        zoom: 10,
        center: center
      });
      var infowindow = new google.maps.InfoWindow({});
      var bounds = new google.maps.LatLngBounds();
      var marker, count;
      var markers = [];
      for (count = 0; count < locations.length; count++) {
        var label = count + 1;
        var position = new google.maps.LatLng(locations[count][1], locations[count][2]);
        var finalPosition = adjustMarkerPlace(markers, position);
        console.log(position);
        console.log(finalPosition);
        marker = new google.maps.Marker({
          position: finalPosition,
          map: map,
          label: label.toString(),
          title: locations[count][0]
        });
        // Add the marker to the array
        markers.push(marker);
        google.maps.event.addListener(marker, 'click', (function (marker, count) {
          return function () {
            infowindow.setContent(locations[count][0]);
            infowindow.open(map, marker);
          }
        })(marker, count));
        bounds.extend(marker.getPosition());
      }
      map.fitBounds(bounds);
    });
  };

  function adjustMarkerPlace(markers, position) {
    ///get array of markers currently in cluster
    //final position for marker, could be updated if another marker already exists in same position
    var finalPosition = position;
    //check to see if any of the existing markers match the latlng of the new marker
    ///get array of markers currently in cluster
    if (markers.length !== 0) {
      for (let i = 0; i < markers.length; i++) {
        var existingMarker = markers[i];
        var pos = existingMarker.getPosition();

        //check if a marker already exists in the same position as this marker
        if (position.equals(pos)) {
          //update the position of the coincident marker by applying a small multipler to its coordinates
          console.log("si");
          var newLat = position.lat() + (Math.random() / 10000);
          var newLng = position.lng() + (Math.random() / 10000);

          finalPosition = new google.maps.LatLng(newLat, newLng);

        }
      }
    }

    return finalPosition;
  }

  Drupal.behaviors.els_voter = {
    attach: function(context, settings) {
      // Callback initialize maps.
      initialize(settings);
    }
  };

})(jQuery, Drupal);
