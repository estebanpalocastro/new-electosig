<?php

namespace Drupal\els_voter;

use Drupal\Core\Database\Connection;

/**
 * Class VoterStorage.
 */
class VoterStorage {
  
  /**
   * Drupal\Core\Database\database definition.
   *
   * @var \Drupal\Core\Database\database
   */
  protected $database;
  
  protected $voterStorage;
  
  protected $campaign_id;
  
  protected $entityTypeManager;
  
  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $database) {
    $this->database = $database;
    //TODO : cambiar a injection de dependencias y current user current_user.
    $user = \Drupal::service('entity_type.manager')->getStorage('user')->load(\Drupal::currentUser()->id());
    $id_campaign = $user->get('field_campanas')->getValue();
    $id_campaign = array_column($id_campaign, 'target_id');
    $this->campaign_id = (isset($id_campaign[0]) && is_numeric($id_campaign[0])) ? $id_campaign[0] : 0;
    
  }
  
  /**
   * @param $fields
   * @return bool|\Drupal\Core\Database\StatementInterface|int|string|null
   */
  public function addVoter($fields) {
    unset($fields['search_section']);
    // validate if voter exits by campaing_id.
    if (!empty($fields['nid']) && $this->checkVoterByCampaign($fields['nid'])) {
      return 'repeat';
    }
    
    try {
      
      if (!empty($fields['geolocation_map'])) {
        $term = $this->getLocationsCode($fields['geolocation_map']['controls']['location']);
        if (!empty($term)) {
          $fields['location_value'] = $fields['geolocation_map']['controls']['location'];
          // Validate if code is to bogota city code.
          $fields['location_city'] = $term->get('field_id_location')->value;
          $manager = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
          $parents = $manager->loadParents($term->id());
          if (!empty($parents)) {
            $parents = reset($parents);
            $fields['location_state'] = $parents->get('field_id_location')->value;
          }
          else {
            $fields['location_state'] = 0;
          }
          
        }
        $fields['lat'] = $fields['geolocation_map']['lat'];
        $fields['lng'] = $fields['geolocation_map']['lng'];
        unset($fields['geolocation_map']);
      }
      
      if (!empty($fields['referer_id'])) {
        preg_match("/.+\\s\\(([^\\)]+)\\)/", $fields['referer_id'], $matches);
        if ($this->getLeadersByCampaign($matches[1])) {
          $fields['referer_id'] = $matches[1];
        }
      }
      $query = $this->database->insert('els_voter');
      $query->fields($fields);
      $result = $query->execute();
    }
    catch (\Exception $e) {
      \Drupal::logger('els_voter')->error($e->getMessage());
      $result = $e->getCode() == 23000 ? 'repeat' : FALSE;
    }
    
    // Added campaign voter relationship.
    if ($result && $result !== 'repeat') {
      $this->addCampaignVoter($fields['nid']);
    }
    return $result;
  }
  
  /**
   * @param $nid_voter
   * @return bool
   */
  private function checkVoterByCampaign($nid_voter) {
    if ($this->campaign_id) {
      $query = $this->database->select('els_voter_campaign');
      $query->fields('els_voter_campaign', ['id']);
      $query->condition('id_campaign', $this->campaign_id);
      $query->condition('id_voter', $nid_voter);
      return $query->execute()->fetchAll();
    }
    return FALSE;
  }
  
  
  /**
   * @param $city_text
   * @return mixed
   */
  public function getLocationsCode($text_location) {
    $locations = explode(', ', $text_location);
    $manager = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    foreach ($locations as $location) {
      $query = $manager->getQuery()
        ->condition('vid', 'ubicaciones')
        ->condition('name', '%' . $location . '%', 'LIKE');
      $result = $query->execute();
      if (!empty($result)) {
        $term = $manager->load(reset($result));
        return $term;
      }
      
    }
    return NULL;
    
  }
  
  public function updateVoter($id, $values) {
    unset($values['search_section']);
    if (!empty($values['geolocation_map'])) {
  
      $term = $this->getLocationsCode($values['geolocation_map']['controls']['location']);
      if (!empty($term)) {
        $values['location_value'] = $values['geolocation_map']['controls']['location'];
        // Validate if code is to bogota city code.
        $values['location_city'] = $term->get('field_id_location')->value;
        $manager = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
        $parents = $manager->loadParents($term->id());
        if (!empty($parents)) {
          $parents = reset($parents);
          $values['location_state'] = $parents->get('field_id_location')->value;
        }
        else {
          $values['location_state'] = 0;
        }
      }
      $values['lat'] = $values['geolocation_map']['lat'];
      $values['lng'] = $values['geolocation_map']['lng'];
      unset($values['geolocation_map']);
    }
  
    if (!empty($values['referer_id'])) {
      preg_match("/.+\\s\\(([^\\)]+)\\)/", $values['referer_id'], $matches);
      $values['referer_id'] = $matches[1];
    }
    if ($this->update('els_voter', $id, $values)) {
      $fields = [
        'id_voter' => $values['nid'],
        'id_campaign' => $this->campaign_id,
      ];
      
      $this->updateVoterCampaign($values['nid'], $fields);
      return TRUE;
    }
    
  }

  private function updateVoterCampaign($id, $values) {
    $query = $this->database->update('els_voter_campaign');
    $query->fields($values);
    $query->condition('id_voter', $id);
    return $query->execute();
  }
  
  /**
   * @param $id
   * @param $values
   * @return \Drupal\Core\Database\StatementInterface|int|null
   */
  private function update($table, $id, $values) {
    $query = $this->database->update($table);
    $query->fields($values);
    $query->condition('id', $id);
    return $query->execute();
  }
  
  /**
   * @param $id
   */
  public function deleteVoter($id)  {
    $this->deleteVoterCampaign($id);
    $query = $this->database->delete('els_voter');
    $query->condition('id', $id);
    return $query->execute();
  }
  
  public function deleteVoterCampaign($id) {
    $voter = $this->getVoter($id);
    $query = $this->database->delete('els_voter_campaign');
    $query->condition('id_voter', $voter['nid']);
    $query->condition('id_campaign', $this->campaign_id);
    return $query->execute();
  }
  
  // TODO: Check if function is same role with condition leaders or all voters.
  public function getLeadersByCampaign($text){
    $query = $this->database->select('els_voter', 'voter');
    $query->join('els_voter_campaign', 'evc', 'voter.nid = evc.id_voter');
    $query->addField('voter','id');
    $query->addField('voter','name');
    $query->condition('voter.campaign_role', 'leader');
    $query->condition('evc.id_campaign', $this->campaign_id);
    $or = $query->orConditionGroup();
    $or->condition('voter.name', '%' . $this->database->escapeLike($text) . '%', 'LIKE');
    $or->condition('voter.email', $this->database->escapeLike($text), 'LIKE');
    $query->condition($or);
  
    return $query->execute();
  }
  
  public function getRefererByEmail($email) {
    $possible_referer = NULL;
    $result = $this->getLeadersByCampaign($email);
    foreach ($result as $referer) {
      if (!empty($referer)) {
        return $referer;
      }
    }
  }
  
  public function getLocationsVoterByCampaign(){
    $db = $this->database;
    $db->query("SET SESSION sql_mode = ''")->execute();
    $query = $db->select('els_voter', 'voter');
    $query->join('els_voter_campaign', 'evc', 'voter.nid = evc.id_voter');
    $query->fields('voter', [
      'nid',
      'id',
      'name',
      'email',
      'mobile_phone',
      'campaign_role',
      'location_value',
      'referer_id',
      'male_female',
      'lat',
      'lng',
    ]);
    $query->condition('evc.id_campaign', $this->campaign_id);
    $current_user = \Drupal::currentUser();
    $user = \Drupal::entityTypeManager()->getStorage('user')->load($current_user->id());
    if (in_array('colaborador_de_campana', $current_user->getRoles())) {
      $query->condition('evc.uid_user', $user->id());
    }
    // Added conditions.
    $query->groupBy('nid');
    
    return $query->execute()->fetchAll();
  }
  
  public function getVotersByCampaign($header, $search_params = [], $num_rows = 20) {
    $db = $this->database;
    $db->query("SET SESSION sql_mode = ''")->execute();
    $query = $db->select('els_voter', 'voter');
    $query->join('els_voter_campaign', 'evc', 'voter.nid = evc.id_voter');
    $query->fields('voter', [
      'nid',
      'id',
      'name',
      'email',
      'mobile_phone',
      'campaign_role',
      'location_value',
      'referer_id',
    ]);
    $query->condition('evc.id_campaign', $this->campaign_id);
    $current_user = \Drupal::currentUser();
    $user = \Drupal::entityTypeManager()->getStorage('user')->load($current_user->id());
    if (in_array('colaborador_de_campana', $current_user->getRoles())) {
      $query->condition('evc.uid_user', $user->id());
    }
    // Added conditions.
    $query->groupBy('nid');
    if (!empty($search_params)) {
      unset($search_params['page']);
      foreach ($search_params as $field => $param) {
        switch ($field) {
          case 'location_value':
            $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($param);
            $query->condition('voter.location_city', $term->get('field_id_location')->value);
          break;
          case 'referer_id':
            preg_match("/.+\\s\\(([^\\)]+)\\)/", $param, $matches);
            $param = $matches[1];
            $query->condition('voter.' . $field, '%' . $param . '%', 'LIKE');
          break;
          
          case 'nid':
            $query->condition('voter.' . $field, $param, 'IN');
          break;
          default:
            $query->condition('voter.' . $field, '%' . $param . '%', 'LIKE');
          break;
        }
      }
    }
    $query->orderBy('created', 'DESC');
    
    $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($header);
    $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit($num_rows);
    return $pager->execute()->fetchAll();
  }
  
  public function validateVoterExistByCampaign($nid) {
    $query = $this->database->select('els_voter', 'voter');
    $query->join('els_voter_campaign', 'evc', 'voter.nid = evc.id_voter');
    $query->fields('voter', [
      'id',
    ]);
    $query->condition('voter.nid', $nid);
    $query->condition('evc.id_campaign', $this->campaign_id);
    return $query->execute()->fetchAll();
  }
  
  public function getVoter($id = 0, $nid = 0) {
    $query = $this->database->select('els_voter', 'voter');
    $query->join('els_voter_campaign', 'evc', 'voter.nid = evc.id_voter');
    $query->fields('voter', [
      'id',
      'nid',
      'name',
      'nid_place',
      'profession',
      'born_date',
      'referer_id',
      'email',
      'mobile_phone',
      'campaign_role',
      'location_value',
      'male_female',
      'lat',
      'lng'
    ]);
    if ($id && !$nid) {
      $query->condition('voter.id', $id);
      $query->condition('evc.id_campaign', $this->campaign_id);
    }
    
    if ($nid && !$id) {
      $query->condition('voter.nid', $nid);
      $query->condition('evc.id_campaign', $this->campaign_id);
    }
    return $query->execute()->fetchAssoc();
  }
  
  public function addCampaignVoter($nid_voter) {
    $user_id = \Drupal::currentUser()->id();
    $result = FALSE;
    if ($this->campaign_id && $user_id) {
      $fields = [
        'id_voter' => $nid_voter,
        'id_campaign' => $this->campaign_id,
        'uid_user' => $user_id,
      ];
      $query = $this->database->insert('els_voter_campaign');
      $query->fields($fields);
      $result = $query->execute();
    }
    return $result;
  }
  
  public function addUserCampaign($id_campaign, $user_id) {
    if ($id_campaign && $user_id) {
      $fields = [
        'id_campaign' => $id_campaign,
        'uid_user' => $user_id,
      ];
    }
    $query = $this->database->insert('els_user_campaign');
    $query->fields($fields);
    $result = $query->execute();
    
    return $result;
  }
  
  public function checkCampaignUser($id_campaign, $user_id) {
    $query = $this->database->select('els_user_campaign', 'campaign');
    $query->fields('campaign', [
      'id',
    ]);
    $query->condition('campaign.uid_user', $user_id);
    $query->condition('campaign.id_campaign', $id_campaign);
    return $query->execute()->fetchCol();
  }
  
  public function getCurrentCampaignId() {
    return $this->campaign_id;
  }
}
