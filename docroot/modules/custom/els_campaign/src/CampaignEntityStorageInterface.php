<?php

namespace Drupal\els_campaign;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\els_campaign\Entity\CampaignEntityInterface;

/**
 * Defines the storage handler class for Campaign entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Campaign entity entities.
 *
 * @ingroup els_campaign
 */
interface CampaignEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Campaign entity revision IDs for a specific Campaign entity.
   *
   * @param \Drupal\els_campaign\Entity\CampaignEntityInterface $entity
   *   The Campaign entity entity.
   *
   * @return int[]
   *   Campaign entity revision IDs (in ascending order).
   */
  public function revisionIds(CampaignEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Campaign entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Campaign entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\els_campaign\Entity\CampaignEntityInterface $entity
   *   The Campaign entity entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(CampaignEntityInterface $entity);

  /**
   * Unsets the language for all Campaign entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
