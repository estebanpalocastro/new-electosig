<?php

namespace Drupal\els_campaign\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Campaign entity entities.
 */
class CampaignEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
