<?php

namespace Drupal\els_voter\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\els_voter\VoterStorage;
use Symfony\Component\Routing\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Disables access if supplied role is ignored.
 */
class VoterAccess implements AccessInterface {
  
  protected $voterStorage;

  /**
   * Builds a new AvoidIgnoredRoles object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   
  public function __construct(VoterStorage $voter_storage) {
    $this->voterStorage = $voter_storage;
  }
  
  /**
   * {@inheritdoc}
   
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('els_voter.storage')
    );
  }
  +/
  
  /**
   * Checks access if role is ignored.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parametrized route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {

    $parameters = $route_match->getParameters();
    if ($parameters->has('id')) {
      $id_voter = $parameters->get('id');
      $voter_storage = \Drupal::service('els_voter.storage');
      $voter = $voter_storage->getVoter($id_voter);
      if (!$voter) {
        return AccessResult::forbidden();
      }
      $permission_update = AccessResult::allowedIfHasPermission($account, 'update voter access');
      $permission_delete = AccessResult::allowedIfHasPermission($account, 'delete voter access');
      return AccessResult::allowed()->andIf($permission_update)->orIf($permission_delete);
    }
  }

}
