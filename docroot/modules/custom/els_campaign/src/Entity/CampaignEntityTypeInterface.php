<?php

namespace Drupal\els_campaign\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Campaign entity type entities.
 */
interface CampaignEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
