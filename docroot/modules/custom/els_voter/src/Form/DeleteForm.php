<?php

namespace Drupal\els_voter\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\els_voter\VoterStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class DeleteForm.
 */
class DeleteForm extends ConfirmFormBase {

  /**
   * Drupal\els_voter\VoterStorage definition.
   *
   * @var \Drupal\els_voter\VoterStorage
   */
  protected $voterStorage;
  
  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $id;
  
  /**
   * Constructs a new DeleteForm object.
   */
  public function __construct(VoterStorage $els_voter_storage) {
    $this->voterStorage = $els_voter_storage;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('els_voter.storage')
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->voterStorage->deleteVoter($this->id);
    drupal_set_message('El votante se ha eliminado con éxito');
    $form_state->setRedirect('els_voter.voters_form', [], []);
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'els_voter.delete_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL) {
    $this->id = $id;
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('els_voter.voters_form');
  }
  
  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $voter = $this->voterStorage->getVoter($this->id);
    return t('Esta seguro que desea eliminar el votante con cédula %nid?', ['%nid' => $voter['nid']]);
  }
  
}
