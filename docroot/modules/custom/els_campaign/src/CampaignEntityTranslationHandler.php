<?php

namespace Drupal\els_campaign;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for campaign_entity.
 */
class CampaignEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
