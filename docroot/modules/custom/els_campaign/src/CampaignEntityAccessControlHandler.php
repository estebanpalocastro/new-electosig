<?php

namespace Drupal\els_campaign;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Campaign entity entity.
 *
 * @see \Drupal\els_campaign\Entity\CampaignEntity.
 */
class CampaignEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\els_campaign\Entity\CampaignEntityInterface $entity */
    
    $campaing_utility = \Drupal::service('els_campaign.utility');
    $content_is_related_user = $campaing_utility->contentRelatedUser($entity, $account);
    switch ($operation) {
      case 'view':
        $access_result = AccessResult::allowedIf($account->hasPermission('access content') && $entity->isPublished() && $content_is_related_user)
          ->cachePerPermissions()
          ->addCacheableDependency($entity);
        if (!$access_result->isAllowed()) {
          $access_result->setReason("The 'access content' permission is required and the campaign entity must be published.");
        }
        return $access_result;

      case 'update':
        if ($account->hasPermission("edit {$entity->bundle()} campaign") && $content_is_related_user) {
          return AccessResult::allowed()->cachePerPermissions();
        }
  
        return AccessResult::neutral()->setReason("The following permissions are required: 'edit {$entity->bundle()} campaign' OR 'administer campaign entity entities'.");

      case 'delete':
        if ($account->hasPermission("delete {$entity->bundle()} campaign") && $content_is_related_user) {
          return AccessResult::allowed()->cachePerPermissions();
        }
  
        return AccessResult::neutral()->setReason("The following permissions are required: 'delete {$entity->bundle()} campaign' OR 'administer campaign entity entities'.");
  
      default:
        // No opinion.
        return AccessResult::neutral()->cachePerPermissions();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, "create $entity_bundle campaign", 'administer campaign entity entities', 'OR');
  }

}
