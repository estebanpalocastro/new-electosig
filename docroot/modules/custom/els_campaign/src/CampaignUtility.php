<?php

namespace Drupal\els_campaign;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides dynamic permissions for campaign of different types.
 */
class CampaignUtility {
  
  protected $entityTypeManager;
  
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }
  
  /**
   * Returns if entity is related with user.
   *
   * @param EntityInterface $entity
   * @param AccountInterface $account
   * @return boolean
   *   The access to content.
   */
  public function contentRelatedUser(EntityInterface $entity, AccountInterface $account) {
    if (in_array('administrator', $account->getRoles()))
      return TRUE;
    
    $user = $this->entityTypeManager->getStorage('user')->load(\Drupal::currentUser()->id());
    $campaign = $user->get('field_campanas')->entity;
    if (!empty($campaign) && in_array($entity->bundle(), ['itinerario']) && $entity->hasField('field_campanas')) {
      $bundle = $entity->get('field_campanas')->entity;
      if(!empty($bundle) && $bundle->id() == $campaign->id()){
        return TRUE;
      }
    }
    
    return FALSE;
  }
  
  
}
