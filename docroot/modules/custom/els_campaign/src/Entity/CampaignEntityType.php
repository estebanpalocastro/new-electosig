<?php

namespace Drupal\els_campaign\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Campaign entity type entity.
 *
 * @ConfigEntityType(
 *   id = "campaign_entity_type",
 *   label = @Translation("Campaign entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\els_campaign\CampaignEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\els_campaign\Form\CampaignEntityTypeForm",
 *       "edit" = "Drupal\els_campaign\Form\CampaignEntityTypeForm",
 *       "delete" = "Drupal\els_campaign\Form\CampaignEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\els_campaign\CampaignEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "campaign_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "campaign_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/campaign_entity_type/{campaign_entity_type}",
 *     "add-form" = "/admin/structure/campaign_entity_type/add",
 *     "edit-form" = "/admin/structure/campaign_entity_type/{campaign_entity_type}/edit",
 *     "delete-form" = "/admin/structure/campaign_entity_type/{campaign_entity_type}/delete",
 *     "collection" = "/admin/structure/campaign_entity_type"
 *   }
 * )
 */
class CampaignEntityType extends ConfigEntityBundleBase implements CampaignEntityTypeInterface {

  /**
   * The Campaign entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Campaign entity type label.
   *
   * @var string
   */
  protected $label;

}
