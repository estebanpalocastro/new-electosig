<?php

namespace Drupal\els_campaign;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\els_campaign\Entity\CampaignEntityInterface;

/**
 * Defines the storage handler class for Campaign entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Campaign entity entities.
 *
 * @ingroup els_campaign
 */
class CampaignEntityStorage extends SqlContentEntityStorage implements CampaignEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(CampaignEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {campaign_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {campaign_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(CampaignEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {campaign_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('campaign_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
