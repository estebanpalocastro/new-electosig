<?php

namespace Drupal\els_campaign\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Campaign entity entities.
 *
 * @ingroup els_campaign
 */
class CampaignEntityDeleteForm extends ContentEntityDeleteForm {


}
