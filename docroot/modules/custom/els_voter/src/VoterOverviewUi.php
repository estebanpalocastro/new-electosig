<?php
/**
 * Created by PhpStorm.
 * User: esteban.palomino
 * Date: 6/05/2019
 * Time: 9:55 AM
 */
namespace Drupal\els_voter;

use Drupal\Core\Form\FormStateInterface;

class VoterOverviewUi{
  
  /**
   * Builds the overview form for the source entities.
   *
   * @param array $form
   *   Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $type
   *   Entity type.
   *
   * @return array
   *   Drupal form array.
   */
  public function overviewForm(array $form, FormStateInterface $form_state) {
    $search_property_params = array_filter(\Drupal::request()->query->all());
    
    
    $form['items'] = array(
      '#type' => 'tableselect',
      '#header' => $this->overviewFormHeader(),
      '#empty' => $this->t('No source items matching given criteria have been found.'),
    );
    
    return $form;
  }
  
  /**
   * Gets overview form header.
   *
   * @return array
   *   Header array definition as expected by theme_tablesort().
   */
  public function overviewFormHeader() {
    $header = [
      ['data' => $this->t('Cédula'), 'field' => 'nid'],
      ['data' => $this->t('Nombres'), 'field' => 'name'],
      ['data' => $this->t('Apellidos'), 'field' => 'last_name'],
      ['data' => $this->t('Correo electronico'), 'field' => 'email'],
      ['data' => $this->t('Celular'), 'field' => 'mobile_phone'],
      ['data' => $this->t('Rol'), 'field' => 'campaign_role'],
      ['data' => $this->t('Ubicacion'), 'field' => 'location_value'],
      ['data' => $this->t('operations'), 'field' => 'operations'],
    ];
    
    return $header;
  }

}