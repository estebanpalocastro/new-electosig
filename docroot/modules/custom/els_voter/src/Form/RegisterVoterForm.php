<?php

namespace Drupal\els_voter\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\els_voter\VoterStorage;
use Drupal\user\PrivateTempStoreFactory;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RegisterVoterForm.
 */
class RegisterVoterForm extends FormBase {
  
  /**
   * @var VoterStorage
   */
  protected $voterStorage;
  
  /**
   * @var
   */
  protected $id;
  
  /**
   * @var
   */
  protected $voterSearch;
  
  /**
   * @var \Drupal\user\PrivateTempStore
   */
  protected $tempStore;
  
  /**
   * {@inheritdoc}
   */
  public function __construct(
    VoterStorage $voter_storage,
    PrivateTempStoreFactory $temp_store_factory
  ) {
    $this->voterStorage = $voter_storage;
    $this->tempStore = $temp_store_factory->get('voter');
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('els_voter.storage'),
      $container->get('user.private_tempstore')
    );
  }
  
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'register_voter_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!$form_state->isSubmitted()) {
      $this->tempStore->set('search_active', FALSE);
    }
    $voter = !empty($this->tempStore->get('voter_search')) ? $this->tempStore->get('voter_search') : [];
    
    if (!empty($this->getRequest()->attributes->get('id'))) {
      $this->id = $this->getRequest()->attributes->get('id');
      $voter = $this->voterStorage->getVoter($this->id);
    }
    /*else {
      $form['search_section'] = [
        '#type' => 'details',
        '#title' => $this->t('Buscador'),
        '#tree' => TRUE,
      ];
      $form['search_section']['nid'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Identificacion'),
        '#placeholder' => $this->t('Ingrese el número de cédula del votante que desea buscar'),
        '#description' => $this->t('Ingrese el número de identificación'),
      ];
      $form['search_section']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('buscar registro'),
        '#submit' => ['::submitSearch'],
        '#validate' => ['::validateSearch'],
      ];
    }*/
    $this->tempStore->set('voter_search', []);
    
    $form['#tree'] = TRUE;
    $form['#prefix'] = '<div id="voter-wrapper">';
    $form['#suffix'] = '</div>';
    $form['#attributes']['novalidate'] = '';
  
    $form['campaign_role'] = [
      '#type' => 'select',
      '#title' => $this->t('Rol de camapaña'),
      '#title_display' => 'none',
      '#options' => [
        'leader' => $this->t('Colaborador'),
        'voter' => $this->t('Simpatizante'),
      ],
      '#empty_option' => 'Rol campaña',
      '#required' => TRUE,
      '#default_value' => isset($voter['campaign_role']) ? $voter['campaign_role'] : NULL,
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombres y Apellidos'),
      '#title_display' => 'none',
      '#placeholder' => $this->t('Nombres y apellidos'),
      '#maxlength' => 255,
      '#size' => 64,
      '#required' => TRUE,
      '#default_value' => !empty($voter['name']) ? $voter['name'] : NULL,
  
    ];
    $form['nid'] = [
      '#type' => 'number',
      '#title' => $this->t('Número de cédula'),
      '#title_display' => 'none',
      '#placeholder' => $this->t('Número de cédula'),
      '#maxlength' => 10,
      '#size' => 30,
      '#required' => TRUE,
      '#default_value' => !empty($voter['nid']) ? $voter['nid'] : NULL,
    
    ];
    if (!empty($voter) && !empty($voter['nid_place'])) {
      $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($voter['nid_place']);
      $form['nid_place']['#default_value'] = $term;
    }
    $form['mobile_phone'] = [
      '#type' => 'tel',
      '#title' => $this->t('Télefono de contacto'),
      '#title_display' => 'none',
      '#placeholder' => $this->t('Télefono de contacto'),
      '#size' => 40,
      '#required' => TRUE,
      '#default_value' => isset($voter['mobile_phone']) ? $voter['mobile_phone'] : NULL,
    ];
    $form['nid_place'] = [
      '#placeholder' => $this->t('Lugar de expedición del documento'),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      "#selection_handler" => "views",
      "#selection_settings" => [
        "view" => [
          "view_name" => "locations_views",
          "display_name" => "entity_reference_1",
          "arguments" => [],
        ],
        "match_operator" => "CONTAINS",
      ],
      '#maxlength' => 1024,
      '#size' => 60,
    ];
    $form['male_female'] = [
      '#type' => 'radios',
      '#title' => $this->t('Sexo:'),
      '#options' => ['female' => 'Femenino', 'male' => 'Masculino'],
      '#default_value' => !empty($voter['male_female']) ? $voter['male_female'] : NULL,
    ];
    $form['born_date'] = [
      '#type' => 'date',
      '#description' => $this->t('Ingrese la fecha de nacimiento'),
      '#title' => $this->t('Fecha de nacimiento'),
      '#title_display' => 'before',
      '#date_date_format' => 'Y-m-d',
      '#default_value' => !empty($voter['born_date']) ? $voter['born_date'] : NULL,
    ];
    $form['profession'] = [
      '#type' => 'entity_autocomplete',
      '#placeholder' => $this->t('Profesión u oficio'),
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => [
          'profesiones_u_oficion' => 'profesiones_u_oficion',
        ],
      ],
      '#maxlength' => 1024,
      '#size' => 60,
    ];
    if (!empty($voter) && !empty($voter['profession'])) {
      $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($voter['profession']);
      $form['profession']['#default_value'] = $term;
    }
    $form['email'] = [
      '#type' => 'email',
      '#placeholder' => $this->t('Correo electronico'),
      '#default_value' => !empty($voter['email']) ? $voter['email'] : NULL,
    ];
    $form['referer_id'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Escribe el nombre de la persona y selecciona del listado.'),
      '#placeholder' => $this->t('Referido por'),
      '#autocomplete_route_name' => 'els_voter.leaders_by_campaign.autocomplete',
    ];
    $current_user =\Drupal::currentUser();
    if (
      $current_user->isAuthenticated() &&
      in_array('colaborador_de_campana', $current_user->getRoles())
    ) {
      $possible_referer = $this->voterStorage->getRefererByEmail($current_user->getEmail());
      if (!empty($possible_referer)) {
        $form['referer_id']['#default_value'] = $possible_referer->name . ' (' . $possible_referer->id . ')';
      }
      
    }
    if (!empty($voter['referer_id'])) {
      $leader = $this->voterStorage->getVoter($voter['referer_id']);
      $form['referer_id']['#default_value'] = $leader['name'] . ' (' . $leader['id'] . ')';
    }
    $config = \Drupal::config('geolocation.settings');
    $key_google = $config->get('google_map_api_key');
    
    $form['geolocation_map'] = [
      '#type' => 'fieldset',
      '#attributes' => [
        'class' => ['canvas-field-geolocation-map'],
      ],
      '#title' => $this->t('Ubicacion'),
      '#title_display' => 'before',
      '#required' => TRUE,
      
      'lat' => [
        '#type' => 'hidden',
        '#default_value' => !empty($voter['lat']) ? $voter['lat'] : '',
        '#attributes' => [
          'class' => ['geolocation-hidden-lat'],
        ],
      ],
      'lng' => [
        '#type' => 'hidden',
        '#default_value' => !empty($voter['lng']) ? $voter['lng'] : '',
        '#attributes' => [
          'class' => [
            'geolocation-hidden-lng'
          ]
        ]
      ],
      'description' => [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'geocoder-description-geolocation-map'
        ],
        'description_1' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#attributes' => [
            'class' => ['text-description'],
          ],
          '#value' => $this->t('1. Ingresa: Municipio, Departamento.'),
        ],
        'description_2' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#attributes' => [
            'class' => ['text-description'],
          ],
          '#value' => $this->t('2. Da click en el mapa para dar una ubicación más precisa.'),
        ],
      ],
      'controls' => [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'geocoder-controls-wrapper-field-geolocation-map',
          'class' => ['geocode-controls-wrapper'],
        ],
        'location' => [
          '#type' => 'textfield',
          '#placeholder' => t('Ingrese Municipio, Departamento'),
          '#title' => $this->t('Ubicacion'),
          '#title_display' => 'none',
          '#required' => TRUE,
          '#default_value' => !empty($voter['location_value']) ? $voter['location_value'] : NULL,
          '#attributes' => [
            'class' => [
              'location',
              'form-autocomplete'
            ]
          ],
          '#theme_wrappers' => []
        ],
        'search' => [
          '#type' => 'html_tag',
          '#tag' => 'button',
          '#attributes' => [
            'class' => ['search'],
            'title' => t('Buscar'),
          ]
        ],
        'locate' => [
          '#type' => 'html_tag',
          '#tag' => 'button',
          '#attributes' => [
            'class' => ['locate'],
            'style' => 'display: none;',
            'title' => t('Ubicar'),
          ]
        ],
        'clear' => [
          '#type' => 'html_tag',
          '#tag' => 'button',
          '#attributes' => [
            'class' => ['clear', 'disabled'],
            'title' => t('borrar'),
          ],
        ]
      ],
      'map_canvas' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'id' => 'field-geolocation-map',
          'class' => ['geolocation-map-canvas'],
        ],
        '#attached' => [
          'library' => [
            'geolocation/geolocation.widgets.googlegeocoder',
            ],
          'drupalSettings' => [
            'geolocation' => [
              'widgetSettings' => [
                'field-geolocation-map' => [
                  'autoClientLocation' => !empty($voter['location_value']) ? FALSE : TRUE,
                  'autoClientLocationMarker' => !empty($voter['location_value']) ? FALSE : TRUE,
                ]
              ],
              'widgetMaps' => [
                'field-geolocation-map' => [
                  'lat' => !empty($voter['lat']) ? $voter['lat'] : '',
                  'lng' => !empty($voter['lng']) ? $voter['lng'] : '',
                  'settings' => [
                    'google_map_settings' => [
                      'type' => 'ROADMAP',
                      'zoom' => !empty($voter) ? 14 : 10,
                      'minZoom' => 0,
                      'maxZoom' => 18,
                      'rotateControl' => 0,
                      'mapTypeControl' => 1,
                      'streetViewControl' => 0,
                      'zoomControl' => 1,
                      'fullscreenControl' => 0,
                      'scrollwheel' => 1,
                      'disableDoubleClickZoom' => 0,
                      'draggable' => 1,
                      'height' => '400px',
                      'width' => '100%',
                      'info_auto_display' => 1,
                      'marker_icon_path' => '',
                      'disableAutoPan' => 0,
                      'style' => '',
                      'preferScrollingToZooming' => 0,
                      'gestureHandling' => 'auto',
                    ],
                    'populate_address_field' => '',
                    'target_address_field' => null,
                    'explicite_actions_address_field' => '',
                    'default_longitude' => null,
                    'default_latitude' => null,
                    'auto_client_location' => '',
                    'auto_client_location_marker' => '',
                    'allow_override_map_settings' => 0,
                  ]
                ]
              ],
              'google_map_url' => 'https://maps.googleapis.com/maps/api/js?libraries=&region=Colombia&language=ES&v=&client=&callback=Drupal.geolocation.googleCallback&key=' . $key_google . '&components=country:co',
            ]
          ]
        ]
      ],
    ];
    $form['term_and_conditions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Politica de protección de datos'),
      '#required' => TRUE,
      '#default_value' => !empty($voter) ? TRUE : FALSE,
    ];
    $form['created'] = [
      '#type' => 'number',
      '#disabled' => (bool)TRUE,
      '#default_value' => time(),
      '#access' => FALSE
    ];
    
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Crear votante'),
    ];
    
    if (!empty($voter) && !empty($this->getRequest()->attributes->get('id'))) {
      // Added update action
      $form['submit']['#value'] = $this->t('Actualizar votante');
      $form['submit']['#submit'] = ['::submitUpdate'];
      
      $url_redirect = Url::fromRoute('els_voter.delete_form', ['id' => $this->id]);
      $form['delete'] = [
        '#type' => 'link',
        '#url' => $url_redirect,
        '#title' => $this->t('Eliminar'),
      ];
    }
    
    
    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if ($form_state->hasValue(['nid']) && !is_numeric($form_state->getValue('nid'))) {
      $form_state->setErrorByName('nid', $this->t('El campo identificacion debe ser númerico'));
    }
    $form_state->cleanValues();
    $values = $form_state->getValues();
    if ($values['campaign_role'] === 'leader') {
    
      if (empty($values['email'])) {
        $form_state->setErrorByName('email', $this->t('El campo correo electrónico es obligatorio para los colaboradores.'));
      }
      
      if (empty($this->getRequest()->attributes->get('id')) && !empty($this->checkUserExists($values))) {
        $message = $this->t('Por favor ingrese otro correo electrónico para registrar el usuario. Ya se encuentra registrado en el sistema un usuario con este mismo correo.');
        $form_state->setErrorByName('email', $message);
      }
    }
    
    if (!empty($values['geolocation_map']['controls']['location'])) {
      $text_location = $values['geolocation_map']['controls']['location'];
      $locations = explode(', ', $text_location);
      if (count($locations) < 2) {
        $form_state->setErrorByName('geolocation_map.controls.location', $this->t('Por favor ingrese la ubicación completa agregandole el nombre del departamento Departamento, Colombia'));
      }
    }
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateSearch(array &$form, FormStateInterface $form_state) {
    $form_state->clearErrors();
    $search_section = $form_state->getValue('search_section');
    if (!empty($search_section['nid']) && !is_numeric($search_section['nid'])) {
      $form_state->setErrorByName('search_section.nid', $this->t('El campo identificacion del buscador debe ser númerico'));
    }
    elseif (!empty($search_section['nid']) && $this->voterStorage->validateVoterExistByCampaign($search_section['nid'])) {
      $form_state->setErrorByName('search_section.nid', $this->t('El usuario con identificación número %nid ya se encuentra registrado en la campaña', ['%nid' => $search_section['nid']]));
    }
    
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    $form_state->cleanValues();
    $values = $form_state->getValues();
    
    // TODO: Uncomment validation whe use search user.
    //if ($this->tempStore->get('search_active')) {
    //  $result = $this->voterStorage->addCampaignVoter($values['nid']);
    //}
    $result = $this->voterStorage->addVoter($values);
    if ($result == 'repeat') {
      drupal_set_message('No se puede agregar el votante con identificación ' . $values['nid'] . '  porque ya esta registrado en la campaña', 'error');
      $form_state->setRebuild();
    }
    elseif ($result == 'error' || !$result) {
      drupal_set_message('Ha ocurrido un error vuelva a intentarlo más tarde');
      $form_state->setRebuild();
    }
    else {
      //TODO: Add user drupal campaign.
      if ($values['campaign_role'] == 'leader') {
        $this->createUser($values);
        //drupal_set_message('Debe activar el usuario registrado y puede ingresar al sistema con el correo electrónico ' . $values['email'] . ' y el password es (electo_12345)');
      }
      drupal_set_message('Se ha registrado el votante con éxito');
    }
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitUpdate(array &$form, FormStateInterface $form_state) {
    // Display result.
    $form_state->cleanValues();
    $values = $form_state->getValues();
    $result = $this->voterStorage->updateVoter($this->id, $values);
    if (!$result) {
      drupal_set_message('Ha ocurrido un error vuelva a intentarlo mas tarde');
      $form_state->setRebuild();
    }
    else {
      drupal_set_message('Se ha actualizado la informacion con exito');
      $form_state->setRedirect('els_voter.voters_form', [], []);
    }
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitSearch(array &$form, FormStateInterface $form_state) {
    // Display result.
    $nid = $form_state->getValue('search_section');
    $voter = $this->voterStorage->getVoter(0, $nid['nid']);
    unset($voter['referer_id']);
    $this->tempStore->set('voter_search', $voter);
    $this->tempStore->set('search_active', TRUE);
    
  }
  
  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function submitSearchAjax(array &$form, FormStateInterface $form_state) {
    return $form;
  }
  
  /**
   * Validate if user exist in the system.
   *
   * @param array $values
   *   The values of form filled.
   * @return \Drupal\Core\Entity\EntityInterface[]
   * @throws InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function checkUserExists(array $values) {
    $users = \Drupal::entityTypeManager()
      ->getStorage('user')
      ->loadByProperties([
        'mail' => $values['email'],
      ]);
    return $users;
  }
  
  /**
   * Create new user drupal associated to campaign id.
   *
   * @param $values
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createUser($values) {
    $user = User::create();
  
    //Mandatory settings
    $user->setPassword('electo_12345');
    $user->enforceIsNew();
    $user->setEmail($values['email']);
  
    //This username must be unique and accept only a-Z,0-9, - _ @ .
    $user->setUsername($values['email']);
  
    //Optional settings
    $language = 'es';
    $user->set("init", $values['email']);
    $user->set("langcode", $language);
    $user->set("preferred_langcode", $language);
    $user->set("preferred_admin_langcode", $language);
    $user->set("field_nombres", $values['name']);
    // Add location id.
    $term = $this->voterStorage->getLocationsCode($values['geolocation_map']['controls']['location']);
    if (!empty($term)) {
      $user->set("field_location", $term->get('field_id_location')->value);
    }
  
    $user->set("field_campanas", $this->voterStorage->getCurrentCampaignId());
    $user->set("field_telefono", $values['mobile_phone']);
    $user->addRole('colaborador_de_campana');
    $user->block();
    // Save user.
    $user->save();
  }
  
}

