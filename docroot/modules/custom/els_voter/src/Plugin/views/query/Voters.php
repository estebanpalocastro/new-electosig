<?php
namespace Drupal\els_voter\Plugin\views\query;

use Drupal\els_voter\VoterStorage;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;
/**
 * Voters views query plugin which wraps calls to the Voters in order to
 * expose the results to views.
 *
 * @ViewsQuery(
 *   id = "els_voter_view",
 *   title = @Translation("Votantes"),
 *   help = @Translation("Query para los votantes de la campaña.")
 * )
 */
class Voters extends QueryPluginBase {
  
  protected $voterStorage;
  
  /**
   * Voters constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param VoterStorage $voter_storage
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    VoterStorage $voter_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->voterStorage = $voter_storage;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('els_voter.storage')
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public function execute(ViewExecutable $view) {
    $voters = $this->voterStorage->getLocationsVoterByCampaign();
    $index = 0;
    foreach ($voters as $key => $voter) {
      $leader_name = '';
      if (!empty($voter->referer_id)) {
        $leader = $this->voterStorage->getVoter($voter->referer_id);
        $leader_name = $leader['name'];
      }
      $gender = '';
      if ($voter->male_female) {
        $gender =  ($voter->male_female == 'male') ? $this->t('Masculino') : $this->t('Femenino');
      }
      
      $row['nid'] = $voter->nid;
      $row['name'] = $voter->name;
      $row['mobile_phone'] = $voter->mobile_phone;
      $row['campaign_role'] = ($voter->campaign_role == 'leader') ? $this->t('Lider') : $this->t('Simpatizante');
      $row['location_value'] = $voter->location_value;
      $row['referer_id'] = $leader_name;
      $row['male_female'] = $gender;
      $row['email'] = $voter->email;
      $row['index'] = $key;
      $view->result[] = new ResultRow($row);
    }
  }
  
  public function ensureTable($table, $relationship = NULL) {
    return '';
  }
  public function addField($table, $field, $alias = '', $params = array()) {
    return $field;
  }
}
