<?php

namespace Drupal\integration_front\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends FormBase {
  
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'integration_front_settings_form';
  }
  
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'integration_front.settings',
    ];
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_state->disableCache();
    
    $config = $this->config('integration_front.settings');
    
    $imageid = $config->get('background_image');
    if ($imageid) {
      $file = \Drupal\file\Entity\File::load($imageid[0]);  // File Load
      if (!empty($file)) {
        $fileUrl = $file->getFileUri();
        // check if image is valid.
        $image = \Drupal::service('image.factory')->get($fileUrl);
        if ($image->isValid()) {
          $image_render = array(
            '#theme' => 'image_style',
            '#width' => $image->getWidth(),
            '#height' => $image->getHeight(),
            '#style_name' => 'medium',
            '#uri' => $fileUrl,
          );
        }
      }
    }
    $form['background_url'] = [
      '#type'   => 'item',
      '#markup' => render($image_render),
    ];
    $form['background_image'] = [
      '#type' => 'managed_file',
      '#name' => 'Background Image',
      '#title' => $this->t('Image'),
      '#default_value' => $config->get('background_image') ? $config->get('background_image') : '',
      '#description' => $this->t('Upload an image file for the front background page'),
      '#upload_location' => 'public://integration_front/',
      '#multiple' => FALSE,
      '#upload_validators' => [
        'file_validate_is_image' => [],
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => [25600000]
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    $values = $form_state->getValues();
    $imageid = $values['background_image'];
    if (!empty($imageid[0])) {
      $file = \Drupal\file\Entity\File::load($imageid[0]);
  
      if (gettype($file) == 'object') {
        $file->setPermanent();  // FILE_STATUS_PERMANENT;
        $file->save();
      }
  
      $this->configFactory->getEditable('integration_front.settings')
        ->set('background_image', $values['background_image'])
        ->save();
    }
  }
  
}
