<?php
/**
 * @file
 * Views hooks and utility functions.
 */

/**
 * Implements hook_views_data().
 *
 * Dynamically create views integration for any table Data manages.
 */
function els_voter_views_data() {
  $data = [];
  // Base data.
  $data['els_voter']['table']['group'] = t('Votantes');
  $data['els_voter']['table']['base'] = [
    'title' => t('Votantes'),
    'help' => t('Este widget se usa para traer los votantes de la campaña'),
    'query_id' => 'els_voter_view',
  ];
  
  // Fields.
  $data['els_voter']['nid'] = [
    'title' => t('Identificación'),
    'help' => t('Identificación del votante.'),
    'field' => [
      'id' => 'numeric',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
  ];
  $data['els_voter']['name'] = [
    'title' => t('Nombre del votante'),
    'help' => t('El nombre del votante.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
  ];
  $data['els_voter']['campaign_role'] = [
    'title' => t('Role'),
    'help' => t('Role del votante en la campaña.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
  ];
  $data['els_voter']['mobile_phone'] = [
    'title' => t('Celular'),
    'help' => t('Contacto del votante.'),
    'field' => [
      'id' => 'numeric',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
  ];
  $data['els_voter']['location_value'] = [
    'title' => t('Ubicación'),
    'help' => t('Ubicacion del votante.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
  ];
  $data['els_voter']['referer_id'] = [
    'title' => t('Referido'),
    'help' => t('Referido del votante.'),
    'field' => [
      'id' => 'standard',
    ],
  ];
  $data['els_voter']['email'] = [
    'title' => t('Correo electronico'),
    'help' => t('El email del votante.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
  ];
  $data['els_voter']['male_female'] = [
    'title' => t('Sexo del votante'),
    'help' => t('El sexo del votante.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
  ];
  return $data;
}
