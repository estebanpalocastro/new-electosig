<?php

namespace Drupal\els_campaign;

use Drupal\Core\Routing\UrlGeneratorTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\els_campaign\Entity\CampaignEntityType;

/**
 * Provides dynamic permissions for campaign of different types.
 */
class CampaignPermissions {

  use StringTranslationTrait;
  use UrlGeneratorTrait;

  /**
   * Returns an array of campaigns type permissions.
   *
   * @return array
   *   The node type permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function campaignTypePermissions() {
    $perms = [];
    // Generate node permissions for all node types.
    foreach (CampaignEntityType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }

    return $perms;
  }

  /**
   * Returns a list of node permissions for a given node type.
   *
   * @param \Drupal\els_campaign\Entity\CampaignEntityType $type
   *   The node type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(CampaignEntityType $type) {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      "create $type_id campaign" => [
        'title' => $this->t('%type_name: Create new campaign', $type_params),
      ],
      "edit $type_id campaign" => [
        'title' => $this->t('%type_name: Edit campaign', $type_params),
      ],
      "delete $type_id campaign" => [
        'title' => $this->t('%type_name: Delete campaign', $type_params),
      ],
    ];
  }

}
