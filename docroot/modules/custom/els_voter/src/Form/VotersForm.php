<?php

namespace Drupal\els_voter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\els_voter\VoterStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class VotersForm.
 */
class VotersForm extends FormBase {

  /**
   * Drupal\els_voter\VoterStorage definition.
   *
   * @var \Drupal\els_voter\VoterStorage
   */
  protected $voterStorage;
  /**
   * Constructs a new VotersForm object.
   */
  public function __construct(
    VoterStorage $els_voter_storage
  ) {
    $this->voterStorage = $els_voter_storage;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('els_voter.storage')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'voters_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['operations']['nid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Identificacion'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#default_value' => isset($_GET['nid']) ? $_GET['nid'] : NULL,
    ];
    
    $form['operations']['location_value'] = [
      '#title' => $this->t('Ubicación'),
      '#type' => 'entity_autocomplete',
      '#description' => $this->t('Digita y selecciona un municipio de la lista '),
      '#target_type' => 'taxonomy_term',
      "#selection_handler" => "views",
      "#selection_settings" => [
        "view" => [
          "view_name" => "locations_views",
          "display_name" => "entity_reference_1",
          "arguments" => [],
        ],
        "match_operator" => "CONTAINS",
      ],
      '#maxlength' => 1024,
      '#size' => 60,
      '#default_value' => isset($_GET['location_value']) ? $_GET['location_value'] : NULL,
    ];
  
    if (isset($_GET['location_value'])) {
      $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($_GET['location_value']);
      $form['operations']['location_value']['#default_value'] = $term;
    }
    $form['operations']['campaign_role'] = [
      '#type' => 'select',
      '#title' => $this->t('Rol en la campaña'),
      '#empty_option' => $this->t('- Seleccione el rol -'),
      '#empty_value' => '',
      '#options' => ['leader' => $this->t('Colaborador'), 'voter' => $this->t('Simpatizante')],
      '#default_value' => isset($_GET['campaign_role']) ? $_GET['campaign_role'] : NULL,
    ];
    $form['operations']['referer_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Referido por'),
      '#default_value' => isset($_GET['referer_id']) ? $_GET['referer_id'] : NULL,
      '#autocomplete_route_name' => 'els_voter.leaders_by_campaign.autocomplete',
    ];
    $form['operations']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombre'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#default_value' => isset($_GET['name']) ? $_GET['name'] : NULL,
    ];
    $form['operations']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filtrar'),
    ];
  
    $search_property_params = array_filter($this->getRequest()->query->all());
    $form['voters']['#header'] = $this->overviewFormHeader();
    $form['voters']['#type'] ='table';
    $voters = $this->voterStorage->getVotersByCampaign($this->overviewFormHeader(), $search_property_params);
    $links = [];
    $edit_route_name = 'els_voter.edit_form';
    $delete_route_name = 'els_voter.delete_form';
    foreach ($voters as $voter) {
      $links['edit'] = [
        'title' => $this->t('Editar'),
        'url' => Url::fromRoute($edit_route_name, ['id' => $voter->id]),
      ];
      $links['delete'] = [
        'title' => $this->t('Eliminar'),
        'url' => Url::fromRoute($delete_route_name, ['id' => $voter->id]),
      ];
      $leader_name = '';
      if (!empty($voter->referer_id)) {
        $leader = $this->voterStorage->getVoter($voter->referer_id);
        $leader_name = $leader['name'];
      }
      
      $form['voters']['#rows'][] = [
        'referer_id' => $leader_name,
        'campaign_role' => ($voter->campaign_role == 'leader') ? $this->t('Colaborador') : $this->t('Simpatizante'),
        'name' => $voter->name,
        'nid' => $voter->nid,
        'mobile_phone' => $voter->mobile_phone,
        'location_value' => $voter->location_value,
        'operations' => [
          'data' => [
            '#type' => 'operations',
            '#links' => $links,
          ],
        ],
      ];
      
    }
    $form['voters']['#empty'] = $this->t('no se encuentran votantes registrados para la campaña');
    
    $form['pager'] = [
      '#type' => 'pager',
    ];
    return $form;
  }
  
  /**
   * Gets overview form header.
   *
   * @return array
   *   Header array definition as expected by theme_tablesort().
   */
  public function overviewFormHeader() {
    $header = [
      'referer_id' => ['data' => $this->t('Referido')],
      'campaign_role' => ['data' => $this->t('Rol')],
      'name' => ['data' => $this->t('Nombre')],
      'nid' => ['data' => $this->t('Cédula')],
      'mobile_phone' => ['data' => $this->t('Celular')],
      'location_value' => ['data' => $this->t('Ubicacion')],
      'operations' => ['data' => $this->t('Operaciones')],
    ];
    
    return $header;
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    $query = [];
    $form_state->cleanValues();
    $values = $form_state->getValues();
    foreach ($values as $key => $value) {
      $query[$key] = $value;
    }
    unset($query['voters']);
    $form_state->setRedirect('els_voter.voters_form', [], ['query' => $query]);
    return TRUE;
  }

}
