<?php

/**
 * @file
 * Contains campaign_entity.page.inc.
 *
 * Page callback for Campaign entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Campaign entity templates.
 *
 * Default template: campaign_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_campaign_entity(array &$variables) {
  // Fetch CampaignEntity Entity Object.
  $campaign_entity = $variables['elements']['#campaign_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
