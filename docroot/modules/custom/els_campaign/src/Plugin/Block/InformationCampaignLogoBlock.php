<?php

namespace Drupal\els_campaign\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'InformationCampaignBlock' block.
 *
 * @Block(
 *  id = "information_campaign_logo_block",
 *  admin_label = @Translation("Logo de la campaña bloque"),
 * )
 */
class InformationCampaignLogoBlock extends BlockBase implements ContainerFactoryPluginInterface {
  
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  
  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;
  
  /**
   * Constructs a new OkComponentReactBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager object.
   * @param \Drupal\Core\Session\AccountProxy $user_session
   *   The module handler object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    AccountProxy $user_session
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $user_session;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public function build() {
    $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
    $campaign = $user->get('field_campanas')->entity;
    $build = [
      '#theme' => 'els_block_logo_informative',
      '#cache' => ['max-age' => 0],
    ];
    if ($campaign && $campaign->get('field_logo')->entity) {
      $file = $campaign->get('field_logo')->entity;
      $build['#url'] = $file->getFileUri();
      $build['#alt'] = $campaign->get('field_tipo_campana')->getString();
    }
    return $build;
  }

}
