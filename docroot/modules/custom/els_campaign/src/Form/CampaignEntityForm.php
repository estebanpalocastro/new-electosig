<?php

namespace Drupal\els_campaign\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Campaign entity edit forms.
 *
 * @ingroup els_campaign
 */
class CampaignEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\els_campaign\Entity\CampaignEntity */
    $form = parent::buildForm($form, $form_state);
    $form['#attributes']['novalidate'] = '';
    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
    }

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime(REQUEST_TIME);
      $entity->setRevisionUserId(\Drupal::currentUser()->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Se creo %bundle con el título %label con exíto.', [
          '%label' => $entity->label(),
          '%bundle' => $entity->bundle(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Se guardo %bundle con el título %label con exíto.', [
          '%label' => $entity->label(),
          '%bundle' => $entity->bundle(),
        ]));
    }
    $form_state->setRedirect('entity.campaign_entity.canonical', ['campaign_entity' => $entity->id()]);
  }

}
