<?php

namespace Drupal\els_campaign\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Campaign entity entities.
 *
 * @ingroup els_campaign
 */
interface CampaignEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Campaign entity name.
   *
   * @return string
   *   Name of the Campaign entity.
   */
  public function getName();

  /**
   * Sets the Campaign entity name.
   *
   * @param string $name
   *   The Campaign entity name.
   *
   * @return \Drupal\els_campaign\Entity\CampaignEntityInterface
   *   The called Campaign entity entity.
   */
  public function setName($name);

  /**
   * Gets the Campaign entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Campaign entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Campaign entity creation timestamp.
   *
   * @param int $timestamp
   *   The Campaign entity creation timestamp.
   *
   * @return \Drupal\els_campaign\Entity\CampaignEntityInterface
   *   The called Campaign entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Campaign entity published status indicator.
   *
   * Unpublished Campaign entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Campaign entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Campaign entity.
   *
   * @param bool $published
   *   TRUE to set this Campaign entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\els_campaign\Entity\CampaignEntityInterface
   *   The called Campaign entity entity.
   */
  public function setPublished($published);

  /**
   * Gets the Campaign entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Campaign entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\els_campaign\Entity\CampaignEntityInterface
   *   The called Campaign entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Campaign entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Campaign entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\els_campaign\Entity\CampaignEntityInterface
   *   The called Campaign entity entity.
   */
  public function setRevisionUserId($uid);

}
