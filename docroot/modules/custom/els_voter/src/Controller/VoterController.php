<?php

namespace Drupal\els_voter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;
use Drupal\els_voter\VoterStorage;
use Drupal\geolocation\GoogleMapsDisplayTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class VoterController.
 */
class VoterController extends ControllerBase {
  
  protected $voterStorage;
  
  /**
   * Google maps url with default parameters.
   *
   * @var string
   */
  public static $GOOGLEMAPSAPIURL = 'https://maps.googleapis.com/maps/api/js';
  
  /**
   * {@inheritdoc}
   */
  public function __construct(VoterStorage $voter_storage)  {
    $this->voterStorage = $voter_storage;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('els_voter.storage')
    );
  }

  /**
   * Hello.
   *
   * @return string
   *   Return Hello string.
   */
  public function autocompleteLeaders(Request $request) {
    $matches = [];
    $result = $this->voterStorage->getLeadersByCampaign($request->query->get('q'));
    if ($result) {
      foreach ($result as $leader) {
        $matches[] = [
          'value' => $leader->name . ' ' . $leader->last_name . ' (' . $leader->id . ')',
          'label' => $leader->name . ' ' . $leader->last_name,
        ];
      }
    }
    
    return new JsonResponse($matches);
  }
  
  /**
   * List of voters by role and campaign.
   *
   * @return array
   *   Return Table with list of versions.
   */
  public function voters() {
    
    $header = [
      ['data' => $this->t('Cédula'), 'field' => 'nid'],
      ['data' => $this->t('Nombres'), 'field' => 'name'],
      ['data' => $this->t('Apellidos'), 'field' => 'last_name'],
      ['data' => $this->t('Correo electronico'), 'field' => 'email'],
      ['data' => $this->t('Celular'), 'field' => 'mobile_phone'],
      ['data' => $this->t('Rol'), 'field' => 'campaign_role'],
      ['data' => $this->t('Ubicacion'), 'field' => 'location_value'],
      ['data' => $this->t('operations'), 'field' => 'operations'],
    ];
    $result = $this->voterStorage->getVotersByCampaign($header);
   
    
    $rows = [];
    $links = [];
    $edit_route_name = 'els_voter.edit_form';
    $delete_route_name = 'els_voter.delete_form';
    
    foreach ($result as $row) {
      // Add links operations.
      $links['edit'] = [
        'title' => $this->t('Editar'),
        'url' => Url::fromRoute($edit_route_name, ['id' => $row->id]),
      ];
      $links['delete'] = [
        'title' => $this->t('Eliminar'),
        'url' => Url::fromRoute($delete_route_name, ['id' => $row->id]),
      ];
      $markup = new Markup();
      $rows[] = [
        'data' => [
          'nid' => $row->nid,
          'name' => $row->name,
          'last_name' => $row->last_name,
          'email' => $row->email,
          'mobile_phone' => $row->mobile_phone,
          'campaign_role' => ($row->campaign_role == 'leader') ? $this->t('Lider') : $this->t('Simpatizante'),
          'location_value' => $row->location_value,
          'operations' => [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ],
        ],
      ];
    }
    // Generate the table.
    $build['voter_table'] = [
      '#theme' => 'table',
      '#cache' => ['max-age' => 0],
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No tiene votantes registrados en la campaña.'),
    ];
    
    // Finally add the pager.
    $build['pager'] = [
      '#type' => 'pager',
    ];
    return $build;
    
  }
  
  public function maps() {
    $voters = $this->voterStorage->getLocationsVoterByCampaign();
    $config = \Drupal::config('geolocation.settings');
    $parameters = [
      'key' => $config->get('google_map_api_key'),
    ];
    $url = Url::fromUri(static::$GOOGLEMAPSAPIURL, [
      'query' => $parameters,
      'https' => TRUE,
    ]);
    //dump($this->getGoogleMapsApiParameters());
    $build = [
      '#theme' => 'voters_maps_template',
      '#locations' => NULL,
      '#google_map_url' => $url->toString(),
      '#attached' => [
        'library' => [
          'els_voter/els_voter.geolocation.maps',
        ],
        'drupalSettings' => [
          'geolocation' => [
            'voters' => $voters,
          ],
        ],
      ],
      '#cache' => ['max-age' => 0],
    ];
    return $build;
  }
  
}
