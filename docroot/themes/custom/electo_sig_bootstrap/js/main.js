(function ($) {
	$(document).ready(function(){
        var ventana_ancho = $(window).width();
        var ventanas_alto = $(".user-logged-in .main-container").height();
        if (ventana_ancho < 700){
			var dest = $(".user-logged-in .main-container > div.row > section").offset().top;
			$("html, body").animate({scrollTop: dest});
		}
		if (ventana_ancho > 1020){
			//ALTO DEL MENU VERTICAL
			if (ventanas_alto > 748){
 				$(".user-logged-in .main-container > .row > aside.col-sm-3").height($(".main-container").height());
 				$(".user-logged-in .main-container > .row > section.col-sm-9").height($(".main-container").height());				
 				$(".user-logged-in .main-container > .row > aside.col-sm-3").css('height','auto');
 				$(".user-logged-in .main-container > .row > section.col-sm-9").css('height','auto');
 				
			}else{
				$(".user-logged-in .main-container").css('height','auto');
				$(".user-logged-in .main-container > .row > aside.col-sm-3").css('height','auto');
 				$(".user-logged-in .main-container > .row > section.col-sm-9").css('height','auto');		
			}
		}
    });
}(jQuery));
