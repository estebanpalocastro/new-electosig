<?php
/**
 * @file
 * Contains \Drupal\custom_user\PathProcessor\UserPassProcessor.
 * 
 * Put this file in "src/PathProcessor" module directory.
 */

namespace Drupal\custom_user\PathProcessor;

use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

/**
 * Processes the outbound path and replace user.pass route to email link.
 */
class UserPassPathProcessor implements OutboundPathProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = array(), Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    if ($path === '/user/password') {
      $site_email = \Drupal::config('system.site')->get('mail');
      $url = Url::fromUri('mailto:' . $site_email)->setOption('query', [
        'subject' => t('Request new password'),
        'body' => t('I forgot my password, please, reset it for me, my user email is: :email', [':email' => empty($options['query']['name']) ? '' : $options['query']['name']]),
      ]);
      $path = $url->toString();
      // Fix useless param in query.
      if (!empty($options['query']['name'])) {
        $path .= '&';
      }
    }

    return $path;
  }
}