<?php

namespace Drupal\els_campaign\Controller;

use Drupal\Core\Controller\ControllerBase;
use \Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\els_voter\VoterStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DefaultController.
 */
class DefaultController extends ControllerBase {
  
  protected $voterStorage;
  
  /**
   * {@inheritdoc}
   */
  public function __construct(VoterStorage $voter_storage)  {
    $this->voterStorage = $voter_storage;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('els_voter.storage')
    );
  }
    
  /**
   * Home.
   *
   * @return string
   *   Return Hello string.
   */
  public function home() {
    $voters = $this->voterStorage->getLocationsVoterByCampaign();
    $campaign_id = $this->voterStorage->getCurrentCampaignId();
    $current_user = \Drupal::currentUser();
    $users = [];
    // Validate permissions access users by role.
    if (array_intersect(['colaborador_de_campana', 'administrator'], $current_user->getRoles()) > 0) {
      $users = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties(
        ['field_campanas' => $campaign_id]
      );
    }
    $itineraries = \Drupal::entityTypeManager()->getStorage('campaign_entity')->loadByProperties([
        'field_campanas' => $campaign_id,
        'type' => 'itinerario',
      ]
    );
    $urlVoters = Url::fromRoute('els_voter.voters_form');
    $linkVoters = Link::fromTextAndUrl('Visitar sección', $urlVoters);
    $urlItinerary = Url::fromUri('internal:/itinerarios-de-campana');
    $linkItinerary = Link::fromTextAndUrl('Visitar sección', $urlItinerary);
    $urlUsers = Url::fromUri('internal:/colaboradores');
    $linkUsers = Link::fromTextAndUrl('Visitar sección', $urlUsers);
    $build = [
      '#theme' => 'els_home_page',
      '#voters_num' => count($voters),
      '#voters_url' => $linkVoters->toRenderable(),
      '#users_num' => count($users),
      '#users_url' => $linkUsers,
      '#itineraries_num' => count($itineraries),
      '#itineraries_url' => $linkItinerary->toRenderable(),
      '#cache' => ['max-age' => 0],
    ];
    return $build;
  }

}
